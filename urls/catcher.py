from django.conf.urls import include, url

from gestalt.web.helpers import Reactor

from uchikoma.connector.views import Prime, apis as APIs
from uchikoma.console.views import console as Dashboard
from uchikoma.landing.views import www as Views

from django.contrib import admin
admin.autodiscover()

#from neo4django import admin as neo_admin
#neo_admin.autodiscover()

################################################################################

urlpatterns = [
    url(r'^auth/',    include('social.apps.django_app.urls', namespace='social')),

    url(r'^auth/email/$',                   APIs.require_email, name='require_email'),
    url(r'^auth/email/sent/',               APIs.validation_sent),
    url(r'^auth/login/$',                   APIs.login,          name='login'),
    url(r'^auth/logout/$',                  APIs.logout,         name='logout'),
    url(r'^auth/done/$',                    Prime.network__list, name='done'),
    url(r'^auth/ajax/(?P<backend>[^/]+)/$', APIs.ajax_auth,      name='ajax-auth'),

    #url(r'^hub/$',                                       Prime.landing),
    #url(r'^hub/identities$',                             Prime.ident_list),
    #url(r'^hub/@(?P<owner>[^/]+)/$',                     Prime.ident_view),
    #url(r'^hub/@(?P<owner>[^/]+)/repos$',                Prime.repo_list),
    #url(r'^hub/@(?P<owner>[^/]+)/dynos$',                Prime.dyno_list),
    #url(r'^hub/@(?P<owner>[^/]+)/backends$',             Prime.backend_list),
    #url(r'^hub/@(?P<owner>[^/]+)/~(?P<narrow>[^/]+)/$',  Prime.repo_view),
    #url(r'^hub/@(?P<owner>[^/]+)/-(?P<narrow>[^/]+)/$',  Prime.dyno_view),
    #url(r'^hub/@(?P<owner>[^/]+)/\+(?P<narrow>[^/]+)/$', Prime.backend_view),

    url(r'^manage/admin/',   include(admin.site.urls)),
    url(r'^manage/queue/',   include('django_rq.urls')),
    url(r'^manage/mongo/',   include('mongonaut.urls')),

    url(r'^$',                Views.homepage),
]

