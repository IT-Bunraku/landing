from django.conf.urls import include, url

from gestalt.web.helpers import Reactor

from uchikoma.landing.views import www as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('www',
    url(r'^legal/(?P<path>[^/]+)$', Views.homepage,      name='legal_page'),

    url(r'^(?P<path>[^/]+)\.html$', Views.homepage,      name='dynamic_page'),

    url(r'^$',                      Views.homepage),
)

