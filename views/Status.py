from uchikoma.landing.shortcuts import *

################################################################################

@render_to('cdn/views/homepage.html')
def homepage(request):
    return context(
        primitives = enumerate_schemas(owner_id=request.user.pk),
    )

#*******************************************************************************

@render_to('status/views/homepage.html')
def general(request):
    return context(
        primitives = enumerate_schemas(owner_id=request.user.pk),
    )

################################################################################

def backends(request):
    resp = Reactor.get_backends_status()

    return JsonResponse(resp)

#*******************************************************************************

def platform(request):
    resp = Reactor.get_platform_status()

    return JsonResponse(resp)

#*******************************************************************************

def clouds(request):
    resp = Reactor.get_cloud_status()

    return JsonResponse(resp)

