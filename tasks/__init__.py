from uchikoma.landing.utils import *

################################################################################

def cleanup():
    for idn in Identity.objects.all():
        cleanup_identity.delay(idn.alias)

#j = schedule.enqueue_at(datetime(2020, 10, 10), cleanup)

#*******************************************************************************

@Reactor.rq.register_task(queue='default')
def cleanup_identity(narrow):
    qs = Identity.objects.filter(alias=narrow)

    if len(qs):
        idn = qs[0]

        for repo in idn.repos.all():
            if repo.alive():
                repo.state = 'sleepy'

        for dyno in idn.dynos.all():
            if dyno.alive():
                dyno.state = 'sleepy'

        for bkn in idn.backends.all():
            if bkn.alive():
                bkn.state = 'sleepy'

################################################################################

@Reactor.rq.register_task(queue='default')
def refresh_heroku(api_token):
    cnx = heroku.Application(token=api_token)

    for app in cnx.apps:
        resp = None

        for idn in Identity.objects.all():
            case = idn.match_heroku(app.name)

            if case:
                dyno, st = idn.dynos.get_or_create(
                    alias = case['alias'],
                )

                dyno.save()

#*******************************************************************************

@Reactor.rq.register_task(queue='default')
def refresh_github(api_token):
    pass

#*******************************************************************************

@Reactor.rq.register_task(queue='default')
def refresh_bitbucket(api_token):
    pass

